using Core.Interfaces;
using Core.Models;
using DAL.Data;
using DAL.Helpers;
using DAL.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static DAL.Data.ApplicationDbContext;

namespace StableSchema
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor();
            services.AddControllers();

            services.AddDbContext<TenantDbContext>(
                options => options.UseSqlServer("name=DatabaseSettings:Default:ConnectionString"));
            services.AddDbContext<ApplicationDbContext>(
                builder => builder.UseSqlServer("name=DatabaseSettings:Default:ConnectionString")
                    .ReplaceService<IModelCacheKeyFactory, SchemaAwareModelCacheKeyFactory>());
            
            services.AddTransient<ITenantService, TenantService>();
            services.AddTransient<IAuthService, AuthService>();
            services.AddTransient<ISchemaService, SchemaService>();
            services.AddTransient<IHorseService, HorseService>();
            services.Configure<DatabaseSettings>(Configuration.GetSection(nameof(DatabaseSettings)));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
