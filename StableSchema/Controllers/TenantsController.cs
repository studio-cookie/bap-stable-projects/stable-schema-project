﻿using Core.DTO;
using Core.Interfaces;
using Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace StableServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TenantsController : ControllerBase
    {
        private readonly ITenantService _tenantService;

        public TenantsController(ITenantService tenantService)
        {
            _tenantService = tenantService;
        }

        //create new tenant
        [HttpPost]
        public async Task<IActionResult> CreateTenantAsync(TenantDTO tenantIn)
        {
            Tenant tenant = new Tenant()
            {
                Name = tenantIn.Name,
                Schema = tenantIn.Schema,
            };
            return Ok(await _tenantService.CreateAsync(tenant));
        }

        [HttpPost("{id}/migrate")]
        public async Task<IActionResult> RunMigrations(int id)
        {
            Tenant tenant = await _tenantService.GetByIdAsync(id);
            await _tenantService.RunMigrations(tenant);
            return Ok();
        }
    }
}
