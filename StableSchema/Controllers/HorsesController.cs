﻿using Core.DTO;
using Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace StableServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HorsesController : ControllerBase
    {
        private readonly IHorseService _service;
        public HorsesController(IHorseService service)
        {
            _service = service;
        }
        [HttpGet]
        public async Task<IActionResult> GetHorsesAsync()
        {
            try
            {
                var Horses = await _service.GetAllAsync();
                return Ok(Horses);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetHorseAsync([FromRoute] int id)
        {
            var HorseDetails = await _service.GetByIdAsync(id);
            return Ok(HorseDetails);
        }

        [HttpPost]
        public async Task<IActionResult> CreateHorseAsync(HorseDTO request)
        {
            return Ok(await _service.CreateAsync(request.Name));
        }
    }
}
