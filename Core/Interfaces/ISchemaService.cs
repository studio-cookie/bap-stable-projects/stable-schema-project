﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Interfaces
{
    public interface ISchemaService
    {
        string Schema { get; }
    }
}
