﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{
    public class Configuration
    {
        public string DBProvider { get; set; }
        public string ConnectionString { get; set; }
    }
}
