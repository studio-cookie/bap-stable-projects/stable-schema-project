﻿using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Models
{
    public class Tenant: BaseEntity, ISchemaService
    {
        public string Name { get; set; }
        public string Schema { get; set; }
    }
}
