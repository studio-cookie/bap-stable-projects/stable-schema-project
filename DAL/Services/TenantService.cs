﻿using Core.Interfaces;
using Core.Models;
using DAL.Data;
using DAL.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
    public class TenantService : ITenantService
    {
        private readonly TenantDbContext _tenantContext;
        private readonly DatabaseSettings _databaseSettings;

        public TenantService(TenantDbContext tenantContext, IOptions<DatabaseSettings> databaseSettings)
        {
            _tenantContext = tenantContext;
            _databaseSettings = databaseSettings.Value;
        }

        public async Task<Tenant> GetByIdAsync(int id)
        {
            return await _tenantContext.Tenants.FindAsync(id);
        }

        public async Task<Tenant> CreateAsync(Tenant tenant)
        {
            _tenantContext.Add(tenant);
            await _tenantContext.SaveChangesAsync();

            return tenant;
        }

        public async Task RunMigrations(Tenant tenant)
        {
            DbContextOptionsBuilder<ApplicationDbContext> optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();

            optionsBuilder
                .UseSqlServer(_databaseSettings.Default?.ConnectionString, 
                    b => b.MigrationsHistoryTable("__EFMigrationsHistory", tenant.Schema))
                .ReplaceService<IModelCacheKeyFactory, SchemaAwareModelCacheKeyFactory>()
                .ReplaceService<IMigrationsAssembly, SchemaAwareMigrationAssembly>();

            ApplicationDbContext context = new ApplicationDbContext(optionsBuilder.Options, tenant);
            await context.Database.GetService<IMigrator>().MigrateAsync();
        }

    }
}
