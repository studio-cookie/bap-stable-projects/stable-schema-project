﻿using Core.Interfaces;

namespace DAL.Services
{
    public class SchemaService : ISchemaService
    {
        public string Schema { get; private set; }

        public SchemaService(IAuthService authService)
        {
            Schema = authService.GetTenant().Schema;
        }
    }
}
