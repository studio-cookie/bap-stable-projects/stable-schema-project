﻿using Core.Interfaces;
using Core.Models;
using DAL.Helpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using static DAL.Data.ApplicationDbContext;

namespace DAL.Data
{
    public class ApplicationDbContext : DbContext, ISchemaService
    {
        public string Schema { get; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, ISchemaService schema) : base(options)
        {
            Schema = schema?.Schema;
        }


        public DbSet<Horse> Horses { get; set; }

        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new HorseEntityConfiguration(Schema));
        }
    }

}
