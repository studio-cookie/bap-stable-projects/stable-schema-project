﻿using Core.Interfaces;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DAL.Migrations.ApplicationDb
{
    public partial class initstable : Migration
    {
        private readonly ISchemaService _schema;

        public initstable(ISchemaService schema)
        {
            _schema = schema ?? throw new ArgumentNullException(nameof(schema));
        }

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: _schema.Schema);

            migrationBuilder.CreateTable(
                name: "Horses",
                schema: _schema.Schema,
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Horses", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Horses",
                schema: _schema.Schema);
        }
    }
}
