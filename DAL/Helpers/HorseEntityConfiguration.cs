﻿using Core.Models;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Helpers
{
    public class HorseEntityConfiguration : IEntityTypeConfiguration<Horse>
    {
        private readonly string _schema;

        public HorseEntityConfiguration(string schema)
        {
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<Horse> builder)
        {
            if (!string.IsNullOrWhiteSpace(_schema))
                builder.ToTable(nameof(ApplicationDbContext.Horses), _schema);

            builder.HasKey(horse => horse.Id);
        }
    }
}
