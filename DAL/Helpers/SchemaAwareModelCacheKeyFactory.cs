﻿using Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using static DAL.Data.ApplicationDbContext;

namespace DAL.Helpers
{
    public class SchemaAwareModelCacheKeyFactory : IModelCacheKeyFactory
    {
        public object Create(DbContext context)
        {
            return new
            {
                Type = context.GetType(),
                Schema = context is ISchemaService schema ? schema.Schema : null
            };
        }
    }
}
